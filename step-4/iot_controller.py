# We need to make sure that python knows about traci
# If you have trouble importing traci, you may need to edit the path in `lib/import_sumo.py`
from lib import import_sumo

# import traci
import traci
import traci.constants as tc

# other dependencies
from time import sleep, time

# Iotery Dependencies
from iotery_embedded_python_sdk import Iotery

# Assumes `sumo-gui` is on the path already.  If not, you will need to update it to include the fully qualified path to your sumo-gui binary
sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "simple.sumocfg",
           "--start", "--time-to-teleport", "100000000"]

# Start the simulation
traci.start(sumoCmd)
step = 0

""" IOTERY SETUP """
# Edit the lines below to set your vehicle identity for the cloud

# Set up Iotery credentials
iotery_delivery_vehicle_serial = "YOUR_DEVICE_SERIAL_GOES_HERE"
iotery_delivery_vehicle_key = "YOUR_DEVICE_KEY_GOES_HERE"
iotery_delivery_vehicle_secret = "YOUR_DEVICE_SECRET_GOES_HERE"

# Get this in the system menu option on the Iotery Dashboard (under Team ID)
iotery_team_id = "YOUR_IOTERY_TEAM_ID"


""""""

# Get the vehicle device from Iotery
delivery_vehicle_cloud_connector = Iotery()
delivery_vehicle_details = delivery_vehicle_cloud_connector.getDeviceTokenBasic(data={"key": iotery_delivery_vehicle_key,
                                                                                      "serial": iotery_delivery_vehicle_serial, "secret": iotery_delivery_vehicle_secret, "teamUuid": iotery_team_id})

# Security step: Sets the token to identify the device (the delivery vehicle)
delivery_vehicle_cloud_connector.set_token(delivery_vehicle_details["token"])

# Gets information about the vehicle
delivery_vehicle = delivery_vehicle_cloud_connector.getMe()

# Begin the simulation.  We set the step time to a high number (10000 seconds) to keep it running for a while
while step < 10000:

    # Get the current epoch time
    t = int(time())

    # Simulate the SUMO step
    traci.simulationStep()

    # Below are some helpful functions that might come in handy

    speed = traci.vehicle.getSpeed("veh1")  # m/s
    current_road = traci.vehicle.getRoadID("veh1")  # current edge name

    # For other available functions on `traci.vehicle`, check out https://sumo.dlr.de/pydoc/traci._vehicle.html

    data = {
        "packets": [{
            "timestamp": t,
            "deviceUuid": delivery_vehicle["uuid"],
            "deviceTypeUuid": delivery_vehicle["deviceTypeUuid"],
            "data":{
                "vehicle_speed": speed,
                "route_edge": current_road
            }
        }]}
    iotery_response_delivery_vehicle = delivery_vehicle_cloud_connector.postData(
        deviceUuid=delivery_vehicle["uuid"], data=data)

    # increase the step for the next iteration
    step += 1

    # slow the simulation down so we can watch it
    sleep(0.1)
